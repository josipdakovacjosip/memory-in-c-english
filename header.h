#ifndef HEADE_H
#define HEADER_H

#include "datatype.h"

int izbornik();	              						 //main menu
int novaIgra(char**, char**, int);	                                //new game  
int provjera(char**, char**, int, int, int, int);		        //checking equality in fields
char* zauzimanjeZn(char*, int);						//dynamic allocation for char array 
char* popunjavanjeZn(char*, int);                                       //filling the game board with characters 
char** zauzimanje(char**, int);						//dynamic allocation m*m array
char rndZnak(char*);							//generating random char
char* mjesanje(char*, int);						//randomizing char array
char** popunjavanjeSkrivene(char**, int);				//filling array with '@'
char** popunjavanje(char*, char**);					//filling array with random characters  --gotovo
void ispis(char**, int);						//printing game board
void unosPodataka(float, int, int);					//filling struct with data and writing in file
void* citanjeIgraca(int);						//reading struct array
void ispisSvih(const IGRAC* const);					//printing all played games
void TopTri(int, char);							//printing top 3 players
void pravila();								//printing rules
void ispisMemory();							//printing title
void sortVrijeme(IGRAC* const, GRAC* const, int);		        //bubble sort by time
void sortKoraci(IGRAC* const, GRAC* const, int);		        //bubble sort by number of steps
int izlaz(char*, char**, char**);					//exit

#endif

