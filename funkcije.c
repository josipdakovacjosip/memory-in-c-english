#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <windows.h>
#include "datatype.h"
//#include "header.h"


int brPog = 0; 		//counter that counts number of found pairs
int brPot = 0;		//counter of steps 
int brIgr = 0;		//number of played games

void ispisMemory()															
{
	system("cls");
	printf("\t  __  __ ______ __  __  ____  _______     __\n");
	printf("\t |  \\/  |  ____|  \\/  |/ __ \\|  __ \\ \\   / /\n");
	printf("\t | \\  / | |__  | \\  / | |  | | |__) \\ \\_/ / \n");
	printf("\t | |\\/| |  __| | |\\/| | |  | |  _  / \\   /  \n");
	printf("\t | |  | | |____| |  | | |__| | | \\ \\  | |   \n");
	printf("\t |_|  |_|______|_|  |_|\\____/|_|  \\_\\ |_|   ");

}

void pravila()																
{
	system("cls");
	ispisMemory();
	printf("\n******************************************************************\n");
	printf("Goal of the game is to find pairs of the same characters \n\
in as little time as possible\
\nTo begin new game choose option no.2 in main menu\n\
and select size of board.\n\
When the game is started you will be shown filled playing board\n\
which will be replaced with board filled with '@'.\n\
\nTo choose which field to show, insert coordinates\n\
in RowColumn format (i.e. b4) and press Enter.\n\
After you input two coordinates those fields will be shown.\n\
If symbols are equal they will stay shown for the rest of the game.\n\
If they are not equal they will be shown for 2 seconds and\n\
'@' char will be written again. \n\
This game was created as a final project for subject Programiranje 2\n\
on Faculty of Electrical Engineering, Computer Science and\nInformation Technology Osijek\n\
\n\nTo return to main menu press any key\n");
	printf("******************************************************************\n");
			
	getch();
	system("cls");
}
///////////////////////////////////////////////////////////////////
char rndZnak(char *pokZn)												
{
	char a;	
	do
	{
		a = (char)rand();		
	}while (((a >= 0 && a <= 32) || a == 127) || a == '@');
	
	return a;
}
///////////////////////////////////////////////////////////////////
char** zauzimanje(char **pokP, int m)										
{
	int i, j;
	int n = sqrt(m);
	pokP = NULL;
	pokP = (char**)calloc((m), sizeof(char*));	
	if(pokP == NULL)
	{
		return NULL;
	}		
	for(i = 0; i < n; i++)
	{
		for(j = 0; j < n; j++)
		{
			*(pokP + i) = (char*)calloc(n, sizeof(char));
			
			if(*(pokP + i) == NULL)
			{
				return NULL;
			}
		}
	}
	return pokP;
}
///////////////////////////////////////////////////////////////////
char* zauzimanjeZn(char *pokZn, int m)		
{
	pokZn = (char*)calloc((m), sizeof(char));	
	return pokZn;
}
///////////////////////////////////////////////////////////////////
char* popunjavanjeZn(char *pokZn, int  m)		
{
	int i, j;
	for(i = 0; i < m; i++)
	{
		*(pokZn + i) = rndZnak(pokZn);
		for(j = 0; j < i; j++)
		{
			if(*(pokZn + i) == *(pokZn + j))
			{
				*(pokZn + i) = rndZnak(
				pokZn);
			}
		}
	}
	return pokZn;
}
///////////////////////////////////////////////////////////////////
char* mjesanje(char *pokZn, int n)				
{
	srand(time (NULL));
	int i, j;
	if(n > 0)
	{
		for(i = n - 1; i > 0; i--)
		{
			j = rand() % (i + 1);
			char t = *(pokZn + j);
			*(pokZn + j) = *(pokZn + i);
			*(pokZn + i) = t;
		}
	}
	return pokZn;
}
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
char** popunjavanjeSkrivene(char **pokPog, int m)							
{	
	int i, j;
	m = sqrt(m);
	for(i = 0; i < m; i++)
	{
		for(j = 0; j < m; j++)
		{
			*(*(pokPog + i)+ j) = '@';
		}
	}
	return pokPog;
}
///////////////////////////////////////////////////////////////////
char** popunjavanje(char *pokZn, char **pokPop, int m)					
{
	char a;
	int i, j;
	int k = 0;
	m = sqrt(m);
	for(i = 0; i < m; i += 2)
	{
		for(j = 0; j < m; j += 2)
		{														//filling even i index and even j index
			a = *(pokZn + k);
			*(*(pokPop + i) + j) = a;
			k++;
		}
	}
	for(i = 1; i < m; i += 2)
	{
		for(j = 1; j < m; j += 2)
		{														//filling odd i index and odd j index
			a = *(pokZn + k);
			*(*(pokPop + i) + j) = a;
			k++;
		}
	}
	if(m == 6)
	{
		pokZn = mjesanje(pokZn, 18);
	}
	else 
	{
		pokZn = mjesanje(pokZn, 8);
	}
	k = 0;
	for(i = 0; i < m; i += 2)
	{															//filling even i index and odd j index
		for(j = 1; j < m; j += 2)
		{
			a = *(pokZn + k);
			*(*(pokPop + i) + j) = a;
			k++;
		}
	}
	for(i = 1; i < m; i += 2)
	{															//filling odd i index and even j 
		for(j = 0; j < m; j += 2)
		{
			a = *(pokZn + k);
			*(*(pokPop + i) + j) = a;
			k++;
		}
	}
	return pokPop;
}
///////////////////////////////////////////////////////////////////
void ispis(char **pokMat, int m)				
{
	int i, j;
	printf("\n");
	for(i = 0; i < m + 1; i++)
	{
		printf("\t\t");
		for(j = 0; j < m + 1; j++)
		{
			if(i == 0)
			{
				printf(" %1d |", j);
			}
			if(j == 0 && i > 0)
			{
				printf(" %1c |", 'a' + i - 1);
			}
			if(i > 0)
			{
				if(j > 0)
				{
					printf(" %1c |", *(*(pokMat + i - 1) + j - 1));
				}
			}
		}
		if(m == 4)
		{
			printf("\n\t\t---|---|---|---|---|\n");
		}
		else
		{
			printf("\n\t\t---|---|---|---|---|---|---|\n");	
		}
		
	}
}
///////////////////////////////////////////////////////////////////
int provjera(char **pokPop, char **pokPog, int x1, int y1, int x2, int y2, int m)	
{																					
	if(*(*(pokPog + x1) + y1) == '@' && *(*(pokPog + x2) + y2) == '@')
	{
		if(*(*(pokPop + x1) + y1) == *(*(pokPop + x2) + y2))
		{
			*(*(pokPog + x1) + y1) = *(*(pokPop + x1) + y1);	//if characters in field are same
			*(*(pokPog + x2) + y2) = *(*(pokPop + x2) + y2);	//leave printed
			ispis(pokPog, m);
			brPog++;
		}
		else
		{
			system("cls");
			ispisMemory();
			printf("\n\n");
			*(*(pokPog + x1) + y1) = *(*(pokPop + x1) + y1);	//if characters in fields are not the same
			*(*(pokPog + x2) + y2) = *(*(pokPop + x2) + y2);	//show them for 3 second and overwrite them with @
			ispis(pokPog, m);
			sleep(2);
			system("cls");
			*(*(pokPog + x1) + y1) = '@';
			*(*(pokPog + x2) + y2) = '@';
			ispis(pokPog, m);
		}
	
		brPot++;
		return brPot;
	}
	else
	{
		printf("\nField is open already!");
	}
}
///////////////////////////////////////////////////////////////////
void unosPodataka(float vrijeme, int korak, int mod)
{
	FILE* pokF;
	
	if (mod == 4)
	{
		pokF = fopen("score_4.bin", "rb+");
	
		if(!pokF)
		{
			perror("Unable to open file.");
			exit(EXIT_FAILURE);
		}
	}
	else
	{
		pokF = fopen("score_6.bin", "rb+");
	
		if(!pokF)
		{
			perror("Unable to open file.");
			exit(EXIT_FAILURE);
		}
	}
	
	IGRAC poljeIgraca = { 0 };
	
	printf("\n\nEnter your name: ");
	scanf("%5s", &poljeIgraca.ime);
	fflush(stdin);
	fseek(pokF, 0, SEEK_SET);
	fread(&brIgr, sizeof(int), 1, pokF);
	poljeIgraca.id = brIgr;
	poljeIgraca.korak = korak;
	poljeIgraca.mod = mod;
	poljeIgraca.vrijeme = vrijeme;
	
	fseek(pokF, sizeof(IGRAC) * brIgr, SEEK_CUR);
	fwrite(&poljeIgraca, sizeof(IGRAC), 1, pokF);
	rewind(pokF);
	brIgr++;
	fwrite(&brIgr, sizeof(int), 1, pokF);
	fclose(pokF);
}
///////////////////////////////////////////////////////////////////
void* citanjeIgraca(int m)
{
	FILE* pokF;
	if(m == 4)
	{
		pokF = fopen("score_4.bin", "rb");
	
		if(!pokF)
		{
			perror("Unable to open file");
			exit(EXIT_FAILURE);
		}
	}
	else
	{
		pokF = fopen("score_6.bin", "rb");
	
		if(!pokF)
		{
			perror("Unable to open file");
			exit(EXIT_FAILURE);
		}
	}
	
	rewind(pokF);
	fread(&brIgr, sizeof(int), 1, pokF);
	
	system("cls");
	ispisMemory();
	printf("\n\n******************************************************************\n");
	printf("\nNumber of played games is %d", brIgr);
	IGRAC* poljeIgraca = (IGRAC*)calloc(brIgr, sizeof(IGRAC));
	
	if(!poljeIgraca)
	{
		perror("allocating field for players");
		exit(EXIT_FAILURE);
	}
	
	fread(poljeIgraca, sizeof(IGRAC), brIgr, pokF);
	return poljeIgraca;
}
///////////////////////////////////////////////////////////////////
void ispisSvih(const IGRAC* const poljeIgraca)
{
	if(!poljeIgraca)
	{
		printf("Field of games is empty.");
		return;
	}
	int i;
	
	for(i = 0; i < brIgr; i++)
	{
		printf("\n\nID: %d\nName: %s\nNumber of moves: %d\nTime: %.3f\nMode: %dx%d",
				(poljeIgraca + i)->id, 
				(poljeIgraca + i)->ime, 
				(poljeIgraca + i)->korak, 
				(poljeIgraca + i)->vrijeme, 
				(poljeIgraca + i)->mod, 
				(poljeIgraca + i)->mod);
	}
	getch();
}

void sortVrijeme(IGRAC* const poljeIgraca, IGRAC* const poljeTopTri, int brIgr)
{
	int i, j;
	IGRAC *temp = (IGRAC*)calloc(1, sizeof(IGRAC));		

	for(i = 0; i < brIgr - 1; i++)
	{
		for(j = 0; j < brIgr - 1 - i; j++)
		{
			if(((poljeIgraca + j + 1)->vrijeme) < (poljeIgraca + j)->vrijeme)
			{
				*temp = *(poljeIgraca + j + 1);
				*(poljeIgraca + j + 1) = *(poljeIgraca + j);
				*(poljeIgraca + j) = *temp;
			}
		}
	}	
	
	for(i = 0; i < 3; i++)
	{
		*(poljeTopTri + i) = *(poljeIgraca + i);
	}
	free(temp);
}

void sortKoraci(IGRAC* const poljeIgraca, IGRAC* const poljeTopTri, int brIgr)
{
	int i, j;
	IGRAC *temp = (IGRAC*)calloc(1, sizeof(IGRAC));		

	for(i = 0; i < brIgr - 1; i++)
	{
		for(j = 0; j < brIgr - 1 - i; j++)
		{
			if(((poljeIgraca + j + 1)->korak) < (poljeIgraca + j)->korak)
			{
				*temp = *(poljeIgraca + j + 1);
				*(poljeIgraca + j + 1) = *(poljeIgraca + j);
				*(poljeIgraca + j) = *temp;
			}
		}
	}	
	
	for(i = 0; i < 3; i++)
	{
		*(poljeTopTri + i) = *(poljeIgraca + i);
	}
	free(temp);
}
///////////////////////////////////////////////////////////////////
void TopTri(int m, char mod)
{
	int i;
	FILE* pokF;
	if(m == 4)
	{
		pokF = fopen("score_4.bin", "rb");
	
		if(!pokF)
		{
			perror("Unable to open file");
			exit(EXIT_FAILURE);
		}	
	}
	else
	{
		pokF = fopen("score_6.bin", "rb");
	
		if(!pokF)
		{
			perror("Unable to open file");
			exit(EXIT_FAILURE);
		}
	}
	
	rewind(pokF);
	fread(&brIgr, sizeof(int), 1, pokF);
	
	IGRAC* poljeIgraca = (IGRAC*)calloc(brIgr, sizeof(IGRAC));
	if(poljeIgraca == NULL)
	{
		perror("allocating memory for poljeIgraca");
		exit(EXIT_FAILURE);
	}
	IGRAC* poljeTopTri = (IGRAC*)calloc(3, sizeof(IGRAC));
	if(poljeTopTri == NULL)
	{
		perror("allocating memory for poljeTopTri");
		exit(EXIT_FAILURE);
	}
	
	fread(poljeIgraca, sizeof(IGRAC), brIgr, pokF);
	if(mod == 'v')
	{
		sortVrijeme(poljeIgraca, poljeTopTri, brIgr);	
	}
	else
	{
		sortKoraci(poljeIgraca, poljeTopTri, brIgr);
	}
	
	system("cls");
	ispisMemory();
	printf("\n\n******************************************************************\n");
	printf("\nTop 3\n");
	for(i = 0; i < 3; i++)
	{
		printf("\n%d.\nID: %d\nName: %s\nNumber of moves: %d\nTime: %.3f sekundi\nMode: %d x %d\n", 
		i + 1, 
		(poljeTopTri + i)->id, 
		(poljeTopTri + i)->ime, 
		(poljeTopTri + i)->korak, 
		(poljeTopTri + i)->vrijeme, 
		(poljeTopTri + i)->mod, 
		(poljeTopTri + i)->mod);
	}
	printf("\nTo return to main menu press any key.");
	getch();
	free(poljeTopTri);
	free(poljeIgraca);
}


///////////////////////////////////////////////////////////////////
int novaIgra(char** pokPog, char**pokPop, int m)		
{															
	char a = NULL, b = NULL;
	int x1 = NULL, y1 = NULL, x2 = NULL, y2 = NULL;
	system("cls");
	ispisMemory();	
	printf("\n\n");
	ispis(pokPop, m);		
	sleep(5);
	system("cls");
	float start, stop, total;
	start = clock();
	do							
	{
		ispisMemory();
		printf("\n\n");
		ispis(pokPog, m);
		fflush(stdin);
	
		printf("\n\nEnter 1st coordinate: ");
		scanf("%c%d%[^\n]", &a, &y1);
		fflush(stdin);
	
		if(a >= 'A' && a <= 'Z')	//conversion from uppercase to lowercase letters
		{
			a += 32;
		}
	
		x1 = a - 97;				//conversion of coordinates
		y1 = y1 - 1;
	
		if(x1 > m || y1 > m || y1 < 0)		//checking if entered coordinate exist
		{						
			printf("\nEnter 2nd coordinate: ");
			sleep(1);
			continue;
		}
	
		printf("\nUnesi drugu koordinatu: ");
		scanf("%c%d%[^\n]", &b, &y2);			
		fflush(stdin);
	
		if(b >= 'A' && b <= 'Z')	
		{
			b += 32;
		}
	
		x2 = b - 97;
		y2 = y2 - 1;
	//	printf("\n1. koordinatta: %d %d\n2. koordinata: %d %d", x1, y1, x2, y2);
	//	sleep(5);
		if(x2 > m || y2 > m || y2 < 0)
		{
			printf("\nInvalid coordinate.");
			sleep(1);
			continue;
		}
		if(x1 == x2 && y1 == y2)
		{
			printf("\nSame coordinates entered.");
			sleep(1);
			continue;
		}
		brPot = provjera(pokPop, pokPog, x1, y1, x2, y2, m);
		system("cls");
	}while(brPog < ((m * m) / 2));
	stop = clock();	
	total = (stop - start) / CLOCKS_PER_SEC;
	brPog = 0;
	ispisMemory();
	printf("\n******************************************************************\n");
	ispis(pokPop, m);
	printf("\nCongratulations, you have completet the game in %d steps and %.2f seconds!", brPot, total);
	getch();
	unosPodataka(total, brPot, m);
	printf("\nTo return to main menu press any key.");
	brPot = 0;
	getch();
}

int izlaz(char* pokZn, char** pokPop, char** pokPog)
{
	char izlaz[3] = "no";
	char odabir[3];
	
	printf("\nAre you sure you want to leave? [yes/no]\n");
	scanf("%2s", odabir);
	if(strcmp(izlaz, odabir) == 0)
	{
		free(pokZn);
		free(pokPop);
		free(pokPog);
		return 0;
	}else
	{
		return 1;
	}
}

int izbornik()							
{
	int i, j, mod2;
	int uvijet = 0;
	int mod = 0;
	
	char znakovi[18];
	char* pokZn = NULL;
	
	char plocaPopunjeno[6][6];													
	char **pokPop = NULL;

	char plocaPogodeno[6][6];														
	char **pokPog = NULL;
	
	static IGRAC* poljeIgraca = NULL;
	static IGRAC* poljePronadenih = NULL;

	FILE* pokF = fopen("score_4.bin", "ab+");
	fread(&brIgr, sizeof(int), 1, pokF);
	if(brIgr == '\0')
	{
		fwrite(&brIgr, sizeof(int), 1, pokF);
	}
	fclose(pokF);

	pokF = fopen("score_6.bin", "ab+");
	fread(&brIgr, sizeof(int), 1, pokF);
	if(brIgr == '\0')
	{
		fwrite(&brIgr, sizeof(int), 1, pokF);
	}
	fclose(pokF);
	
	while(1)
	{
		ispisMemory();
		printf("\n\n******************************************************************\n");
		printf("\tChoose one option:\n");
		printf("******************************************************************\n");
		printf("\n\t1. Rules\n");
		printf("\n\t2. New game\n");
		printf("\n\t3. Top 3\n");
		printf("\n\t4. Show all games\n");
		printf("\n\t5. leave game\n\n");
		printf("******************************************************************\n");
		printf("\t\nOption: ");
		scanf("%d", &uvijet);
		switch (uvijet)
		{
			case 1:
				pravila();
			break;
	
			case 2:
				system("cls");
				ispisMemory();
				printf("\n\n******************************************************************\n");
				printf("\tSelect game mode:\n\
******************************************************************\n\
\n\t1. 4x4 board\n\
\n\t2. 6x6 board\n\
\n\t3. Return to main menu\n\n\
******************************************************************\n\
\nOption: ");
				scanf("%d", &mod);
				switch (mod) 
				{
					case 1:
						pokZn = zauzimanjeZn(pokZn, 8);
						
						for(i = 0; i < 8; i++)
						{
							*(pokZn + i) = rndZnak(pokZn);
							for(j = 0; j < i; j++)
							{
								if(*(pokZn + i) == *(pokZn + j))
								{
									*(pokZn + i) = rndZnak(pokZn);
								}
							}
						}
						pokPop = zauzimanje(pokPop, 16);
						if(pokPop == NULL)
						{
							return 1;
						}
						pokPop = popunjavanje(pokZn, pokPop, 16);
						pokPog = zauzimanje(pokPog, 16);
						if(pokPog == NULL)
						{
							return 1;
						}
						pokPog = popunjavanjeSkrivene(pokPog, 16);
						novaIgra(pokPog, pokPop, 4);
						
						free(pokPog);
						free(pokPop);
						free(pokZn);
					break;
					
					case 2:
						pokZn = zauzimanjeZn(pokZn, 18);
						
						for(i = 0; i < 18; i++)
						{
							*(pokZn + i) = rndZnak(pokZn);
							for(j = 0; j < i; j++)
							{
								if(*(pokZn + i) == *(pokZn + j))
								{
									*(pokZn + i) = rndZnak(pokZn);
								}
							}
						}
						pokPop = zauzimanje(pokPop, 36);
						if(pokPop == NULL)
						{
							return 1;
						}
						pokPop = popunjavanje(pokZn, pokPop, 36);
						pokPog = zauzimanje(pokPog, 36);
						if(pokPog == NULL)
						{
							return 1;
						}
						pokPog = popunjavanjeSkrivene(pokPog, 36);
						novaIgra(pokPog, pokPop, 6);
						
						free(pokPog);
						free(pokPop);
						free(pokZn);
					break;
					
					case 3:
					break;
				}
				
			break;
			
			case 3:
			system("cls");
				int mod1;
				ispisMemory();
				printf("\n\n******************************************************************\n");
				printf("\tChoose which score to show:\n\
******************************************************************\n\
\n\t1. Top 3 4x4 by time\n\
\n\t2. Top 3 6x6 by time\n\
\n\t3. Top 3 4x4 by number of steps\n\
\n\t4. Top 3 6x6 by number of steps\n\
\n\t5. Return to main menu\n\n\
******************************************************************\n\
\nOption: ");
				scanf("%d", &mod1);
				
				switch(mod1)
				{
					case 1:
					TopTri(4, 'v');
					break;
					
					case 2:
					TopTri(6, 'v');
					break;
				
					case 3:
					TopTri(4, 'k');
					break;
					
					case 4:
					TopTri(6, 'k');
					break;
					
					default:
					break;	
				}
				break;
			
			case 4:
				
				system("cls");
				ispisMemory();
				printf("\n\n******************************************************************\n");
				printf("\tSelect game mode:\n\
******************************************************************\n\
\n\t1. All 4x4\n\
\n\t2. All 6x6\n\
\n\t3. Return to main menu\n\n\
******************************************************************\n\
\nOption: ");
				scanf("%d", &mod2);
				switch(mod2)
				{
					case 1:	
						poljeIgraca = (IGRAC*)citanjeIgraca(4);
						if(!poljeIgraca)
						{
							perror("loading field of players");
							exit(EXIT_FAILURE);
						}
						ispisSvih(poljeIgraca);
						free(poljeIgraca);
					break;
				
					case 2:
						poljeIgraca = (IGRAC*)citanjeIgraca(6);
						if(!poljeIgraca)
						{
							perror("loading field of players");
							exit(EXIT_FAILURE);
						}
						ispisSvih(poljeIgraca);
						free(poljeIgraca);
					break;
				
					default:
					continue;
				}
				
				break;
			
			case 5:
			system("cls");
			ispisMemory();
			printf("\n\n******************************************************************\n");
			printf("\n");
			uvijet = izlaz(pokZn, pokPop, pokPog);
			return uvijet;
			break;
		}
	}
	return uvijet;
}


